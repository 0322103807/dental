from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    ClinicasViewSet,
    EmpleadosViewSet,
    MedicosViewSet,
    PacientesViewSet,
    HistorialesViewSet,
    CitasViewSet,
    ReportesViewSet,
    PagosViewSet,
    TratamientosViewSet,
    CitasTratamientosViewSet,
    AlmacenesViewSet,
    SensoresViewSet,
    MaterialesViewSet,
    StocksMaterialesViewSet,
    LoginView,
)

router = DefaultRouter()
router.register(r'clinicas', ClinicasViewSet, basename='clinicas')
router.register(r'empleados', EmpleadosViewSet, basename='empleados')
router.register(r'medicos', MedicosViewSet, basename='medicos')
router.register(r'pacientes', PacientesViewSet, basename='pacientes')  # Especifica el basename aquí
router.register(r'historiales', HistorialesViewSet, basename='historiales')
router.register(r'citas', CitasViewSet, basename='citas')
router.register(r'reportes', ReportesViewSet, basename='reportes')
router.register(r'pagos', PagosViewSet, basename='pagos')
router.register(r'tratamientos', TratamientosViewSet, basename='tratamientos')
router.register(r'citas-tratamientos', CitasTratamientosViewSet, basename='citas-tratamientos')
router.register(r'almacenes', AlmacenesViewSet, basename='almacenes')
router.register(r'sensores', SensoresViewSet, basename='sensores')
router.register(r'materiales', MaterialesViewSet, basename='materiales')
router.register(r'stocks-materiales', StocksMaterialesViewSet, basename='stocks-materiales')

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('', include(router.urls)),
    
]

