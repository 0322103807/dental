from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import (
    Clinicas,
    Empleados,
    Medicos,
    Pacientes,
    Historiales,
    Citas,
    Reportes,
    Pagos,
    Tratamientos,
    CitasTratamientos,
    Almacenes,
    Sensores,
    Materiales,
    StocksMateriales,
)
from .serializers import (
    ClinicasSerializer,
    EmpleadosSerializer,
    MedicosSerializer,
    PacientesSerializer,
    HistorialesSerializer,
    CitasSerializer,
    ReportesSerializer,
    PagosSerializer,
    TratamientosSerializer,
    CitasTratamientosSerializer,
    AlmacenesSerializer,
    SensoresSerializer,
    MaterialesSerializer,
    StocksMaterialesSerializer,
    LoginSerializer,  # Agrega el serializer necesario para el login si es necesario
    UserSerializer,  # Agrega el serializer de usuarios si es necesario
)

# Define tus viewsets para cada modelo

class ClinicasViewSet(viewsets.ModelViewSet):
    queryset = Clinicas.objects.all()
    serializer_class = ClinicasSerializer

class EmpleadosViewSet(viewsets.ModelViewSet):
    queryset = Empleados.objects.all()
    serializer_class = EmpleadosSerializer

class MedicosViewSet(viewsets.ModelViewSet):
    queryset = Medicos.objects.all()
    serializer_class = MedicosSerializer

    def get_queryset(self):
        clinica_id = self.request.query_params.get('clinica', None)
        if clinica_id is not None:
            return Medicos.objects.filter(clinica_id=clinica_id)
        else:
            return Medicos.objects.none()

class PacientesViewSet(viewsets.ModelViewSet):
    queryset = Pacientes.objects.all()
    serializer_class = PacientesSerializer

    def get_queryset(self):
        clinica_id = self.request.query_params.get('clinica', None)
        if clinica_id is not None:
            return Pacientes.objects.filter(clinica_id=clinica_id)
        else:
            return Pacientes.objects.none()

class HistorialesViewSet(viewsets.ModelViewSet):
    queryset = Historiales.objects.all()
    serializer_class = HistorialesSerializer

class CitasViewSet(viewsets.ModelViewSet):
    queryset = Citas.objects.all()
    serializer_class = CitasSerializer
    
    def get_queryset(self):
        medico_id = self.request.query_params.get('medico', None)
        if medico_id is not None:
            return Citas.objects.filter(medico_id=medico_id)
        else:
            return Citas.objects.none()


class ReportesViewSet(viewsets.ModelViewSet):
    queryset = Reportes.objects.all()
    serializer_class = ReportesSerializer
    


class PagosViewSet(viewsets.ModelViewSet):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class TratamientosViewSet(viewsets.ModelViewSet):
    queryset = Tratamientos.objects.all()
    serializer_class = TratamientosSerializer

class CitasTratamientosViewSet(viewsets.ModelViewSet):
    queryset = CitasTratamientos.objects.all()
    serializer_class = CitasTratamientosSerializer

class AlmacenesViewSet(viewsets.ModelViewSet):
    queryset = Almacenes.objects.all()
    serializer_class = AlmacenesSerializer

class SensoresViewSet(viewsets.ModelViewSet):
    queryset = Sensores.objects.all()
    serializer_class = SensoresSerializer

class MaterialesViewSet(viewsets.ModelViewSet):
    queryset = Materiales.objects.all()
    serializer_class = MaterialesSerializer

class StocksMaterialesViewSet(viewsets.ModelViewSet):
    queryset = StocksMateriales.objects.all()
    serializer_class = StocksMaterialesSerializer

class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data
            email = user.email

            try:
                medico = Medicos.objects.get(correo=email)
                is_medico = True
                clinica_id = medico.clinica.id
                medico_id = medico.id  # Obtener el ID del médico
            except Medicos.DoesNotExist:
                is_medico = False
                empleado = Empleados.objects.get(correo=email)
                clinica_id = empleado.clinica.id  # Asumiendo que solo puede ser médico o empleado
                medico_id = None  # No es médico, no hay ID de médico

            response_data = {
                'email': email,
                'is_medico': is_medico,
                'clinica_id': clinica_id,
                'medico_id': medico_id  # Pasar el ID del médico si es médico
            }
            return Response(response_data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
