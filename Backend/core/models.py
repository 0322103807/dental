from django.db import models
# users/models.py
from djongo import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import gettext as _


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Los usuarios deben tener un correo electrónico')
        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(email, password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

class Clinicas(models.Model):
    nombre = models.CharField(max_length=255)
    direccion = models.CharField(max_length=255)

class Empleados(models.Model):
    nombre = models.CharField(max_length=255)
    apPat = models.CharField(max_length=255)
    apMat = models.CharField(max_length=255)
    telefono = models.CharField(max_length=20)
    clinica = models.ForeignKey('Clinicas', on_delete=models.CASCADE)
    correo = models.EmailField(max_length=255)
    contrasenia = models.CharField(max_length=255)
    estado = models.CharField(max_length=255)
    
    def __str__(self):
        return self.correo
    
    def save(self, *args, **kwargs):
        if not User.objects.filter(email=self.correo).exists():
            user = User.objects.create_user(email=self.correo, password=self.contrasenia)
            user.save()
        super(Empleados, self).save(*args, **kwargs)
    
    

class Medicos(models.Model):
    nombre = models.CharField(max_length=255)
    apPat = models.CharField(max_length=255)
    apMat = models.CharField(max_length=255)
    cedula = models.CharField(max_length=255)
    especialidad = models.CharField(max_length=255)
    correo = models.EmailField(max_length=255)
    contrasenia = models.CharField(max_length=255)
    clinica = models.ForeignKey('Clinicas', on_delete=models.CASCADE)
    
    def save(self, *args, **kwargs):
        if not User.objects.filter(email=self.correo).exists():
            user = User.objects.create_user(email=self.correo, password=self.contrasenia)
            user.save()
        super(Medicos, self).save(*args, **kwargs)

class Pacientes(models.Model):
    nombre = models.CharField(max_length=255)
    apPat = models.CharField(max_length=255)
    apMat = models.CharField(max_length=255)
    fechaNacimiento = models.DateField()
    direccion = models.CharField(max_length=255)
    telefono = models.CharField(max_length=20)
    correo = models.EmailField(max_length=255)
    clinica = models.ForeignKey(Clinicas, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.nombre} {self.apPat} {self.apMat}"

class Citas(models.Model):
    fecha = models.DateField()
    hora = models.TimeField()
    tipo = models.CharField(max_length=255)
    
    class Estado(models.TextChoices):
        POR_COMPLETAR = 'Por completar', _('Por completar')
        COMPLETADA = 'Completada', _('Completada')
        CANCELADA = 'Cancelada', _('Cancelada')

    estado = models.CharField(max_length=255, choices=Estado.choices, default=Estado.POR_COMPLETAR)
    paciente = models.ForeignKey(Pacientes, on_delete=models.CASCADE)
    medico = models.ForeignKey(Medicos, on_delete=models.CASCADE)

class Reportes(models.Model):
    hora = models.TimeField()
    fecha = models.DateField()
    notas = models.TextField()
    cita = models.ForeignKey(Citas, on_delete=models.CASCADE)

class Historiales(models.Model):
    paciente = models.ForeignKey(Pacientes, on_delete=models.CASCADE)
    fecha = models.DateField()
    notas = models.TextField()
    reporte = models.ForeignKey(Reportes, on_delete=models.CASCADE)


class Pagos(models.Model):
    fecha = models.DateField()
    monto = models.DecimalField(max_digits=10, decimal_places=2)
    metodoPago = models.CharField(max_length=255)
    cita = models.ForeignKey(Citas, on_delete=models.CASCADE)

class Tratamientos(models.Model):
    nombre = models.CharField(max_length=255)
    descripcion = models.TextField()
    duracion = models.IntegerField()
    costo = models.DecimalField(max_digits=10, decimal_places=2)
    categoria = models.CharField(max_length=255)

class CitasTratamientos(models.Model):
    cita = models.ForeignKey(Citas, on_delete=models.CASCADE)
    tratamiento = models.ForeignKey(Tratamientos, on_delete=models.CASCADE)

class Almacenes(models.Model):
    clinica = models.ForeignKey(Clinicas, on_delete=models.CASCADE)

class Sensores(models.Model):
    tipo = models.CharField(max_length=255)
    ubicacion = models.CharField(max_length=255)
    nombre = models.CharField(max_length=255)
    fechaInstalacion = models.DateField()
    estado = models.CharField(max_length=255)
    almacen = models.ForeignKey(Almacenes, on_delete=models.CASCADE)

class Materiales(models.Model):
    nombre = models.CharField(max_length=255)
    descripcion = models.TextField()
    almacen = models.ForeignKey(Almacenes, on_delete=models.CASCADE)

class StocksMateriales(models.Model):
    material = models.ForeignKey(Materiales, on_delete=models.CASCADE)
    tratamiento = models.ForeignKey(Tratamientos, on_delete=models.CASCADE)
    stock = models.IntegerField()

