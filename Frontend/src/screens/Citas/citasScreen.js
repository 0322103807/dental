import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TextInput, RefreshControl } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { API_URL } from "../../config";
import GreenButton from '../../components/greenButton';

const CitasScreen = () => {
  const [citas, setCitas] = useState([]);
  const [search, setSearch] = useState('');
  const [refreshing, setRefreshing] = useState(false);
  const [clinicaId, setClinicaId] = useState(null); // Estado para almacenar el ID de la clínica
  const [medicoId, setMedicoId] = useState(null); // Estado para almacenar el ID del médico
  const [pacientes, setPacientes] = useState([]);
  const [medicos, setMedicos] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const storedClinicaId = await AsyncStorage.getItem('clinica_id');
        const storedMedicoId = await AsyncStorage.getItem('medico_id'); // Obtener el ID del médico

        console.log('Stored clinicaId:', storedClinicaId);
        console.log('Stored medicoId:', storedMedicoId);

        if (storedClinicaId) {
          setClinicaId(storedClinicaId);
          setMedicoId(storedMedicoId); // Establecer el medicoId solo si está definido

          // Obtener datos de pacientes y médicos
          const pacientesResponse = await axios.get(`http://${API_URL}:8000/api/core/pacientes/?clinica=${storedClinicaId}`);
          const medicosResponse = await axios.get(`http://${API_URL}:8000/api/core/medicos/?clinica=${storedClinicaId}`);

          console.log('Pacientes:', pacientesResponse.data);
          console.log('Medicos:', medicosResponse.data);

          setPacientes(pacientesResponse.data);
          setMedicos(medicosResponse.data);
          
          fetchCitas(storedMedicoId); // Llamar a fetchCitas con el ID del médico
        }
      } catch (error) {
        console.error('Error al obtener datos:', error);
      }
    };

    fetchData(); // Obtener datos al cargar la pantalla
  }, []);

  const fetchCitas = (medicoId) => {
    if (clinicaId) {
      setRefreshing(true); // Activar refreshing mientras se carga la lista
      let url = `http://${API_URL}:8000/api/core/citas/?medico=${medicoId}`;
      
      if (medicoId) {
        url += `&medico=${medicoId}`;
      }

      axios.get(url)
        .then(response => {
          console.log('Citas response:', response.data);
          setCitas(response.data);
          setRefreshing(false); // Desactivar refreshing cuando se complete la carga
        })
        .catch(error => {
          console.error('Error al obtener citas:', error);
          setRefreshing(false); // Asegurarse de desactivar refreshing en caso de error
        });
    }
  };

  const onRefresh = () => {
    fetchCitas(medicoId); // Llamar a fetchCitas para actualizar la lista de citas con el medicoId actual
  };

  const filteredCitas = citas.filter(cita =>
    cita.tipo.toLowerCase().includes(search.toLowerCase())
  );

  const renderItem = ({ item }) => {
    // Encontrar el nombre del paciente
    const paciente = pacientes.find(p => p.id === item.paciente);
    // Encontrar el nombre del médico
    const medico = medicos.find(m => m.id === item.medico);

    return (
      <View style={styles.item}>
        <Text style={styles.title}>{item.tipo}</Text>
        <Text>Fecha: {item.fecha}</Text>
        <Text>Hora: {item.hora}</Text>
        <Text>Estado: {item.estado}</Text>
        {paciente && <Text>Paciente: {paciente.nombre} {paciente.apPat} {paciente.apMat}</Text>}
        {medico && <Text>Médico: {medico.nombre} {medico.apPat} {medico.apMat}</Text>}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TextInput
          style={styles.searchBar}
          placeholder="Buscar cita por tipo"
          value={search}
          onChangeText={setSearch}
        />
        <GreenButton
          title="Nueva Cita"
          onPress={() => navigation.navigate('CreateCita', { onRefresh: fetchCitas })}
        />
      </View>
      <FlatList
        data={filteredCitas}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  searchBar: {
    flex: 1,
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
    marginRight: 10,
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 2,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default CitasScreen;
