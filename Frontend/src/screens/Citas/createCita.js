import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, StyleSheet, Text, TouchableOpacity } from 'react-native';
import axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { API_URL } from "../../config";
import AsyncStorage from '@react-native-async-storage/async-storage';

const CreateCita = ({ navigation, route }) => {
  const { onRefresh } = route.params;
  const [fecha, setFecha] = useState(new Date());
  const [hora, setHora] = useState('');
  const [tipo, setTipo] = useState('');
  const [estado] = useState('Por completar');
  const [pacienteId, setPacienteId] = useState(null);
  const [medicoId, setMedicoId] = useState(null);
  const [pacientes, setPacientes] = useState([]);
  const [medicos, setMedicos] = useState([]);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [selectedPaciente, setSelectedPaciente] = useState(null);
  const [selectedMedico, setSelectedMedico] = useState(null);

  useEffect(() => {
    const fetchClinicaData = async () => {
      try {
        const storedClinicaId = await AsyncStorage.getItem('clinica_id');
        if (!storedClinicaId) {
          console.error('No clinic ID found');
          return;
        }
  
        console.log('Clinic ID:', storedClinicaId);  // Debugging
  
        const pacientesResponse = await axios.get(`http://${API_URL}:8000/api/core/pacientes/?clinica=${storedClinicaId}`);
        const medicosResponse = await axios.get(`http://${API_URL}:8000/api/core/medicos/?clinica=${storedClinicaId}`);
  
        console.log('Pacientes Response:', pacientesResponse.data);  // Debugging
        console.log('Medicos Response:', medicosResponse.data);  // Debugging
  
        setPacientes(pacientesResponse.data);
        setMedicos(medicosResponse.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
  
    fetchClinicaData();
  }, []);
  
  
  const handleCreateCita = () => {
    axios.post(`http://${API_URL}:8000/api/core/citas/`, {
      fecha: fecha.toISOString().split('T')[0],
      hora,
      tipo,
      estado,
      paciente: pacienteId,
      medico: medicoId,
    })
    .then(response => {
      onRefresh();
      navigation.goBack();
    })
    .catch(error => {
      console.error('Error creating cita:', error);
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Fecha:</Text>
      {showDatePicker ? (
        <DateTimePicker
          value={fecha}
          mode="date"
          display="default"
          onChange={(event, selectedDate) => {
            const currentDate = selectedDate || fecha;
            setShowDatePicker(false);
            setFecha(currentDate);
          }}
        />
      ) : (
        <TouchableOpacity onPress={() => setShowDatePicker(true)}>
          <Text style={styles.selectedDate}>{fecha.toDateString()}</Text>
        </TouchableOpacity>
      )}
      <Text style={styles.label}>Hora:</Text>
      <TextInput
        style={styles.input}
        placeholder="Hora"
        value={hora}
        onChangeText={setHora}
      />
      <Text style={styles.label}>Tipo:</Text>
      <TextInput
        style={styles.input}
        placeholder="Tipo"
        value={tipo}
        onChangeText={setTipo}
      />
      <Text style={styles.label}>Paciente:</Text>
      <SearchableDropdown
        onTextChange={(text) => {}}
        onItemSelect={(item) => {
          setPacienteId(item.id);
          setSelectedPaciente(item.name);
        }}
        containerStyle={styles.dropdownContainer}
        textInputStyle={styles.dropdownInput}
        itemStyle={styles.dropdownItem}
        itemTextStyle={styles.dropdownItemText}
        itemsContainerStyle={styles.dropdownItemsContainer}
        items={pacientes.map(paciente => ({
          id: paciente.id,
          name: `${paciente.nombre} ${paciente.apPat} ${paciente.apMat}`
        }))}
        defaultIndex={0}
        placeholder={selectedPaciente || "Selecciona un paciente"}
        resetValue={false}
        underlineColorAndroid="transparent"
      />
      <Text style={styles.label}>Médico:</Text>
      <SearchableDropdown
        onTextChange={(text) => {}}
        onItemSelect={(item) => {
          setMedicoId(item.id);
          setSelectedMedico(item.name);
        }}
        containerStyle={styles.dropdownContainer}
        textInputStyle={styles.dropdownInput}
        itemStyle={styles.dropdownItem}
        itemTextStyle={styles.dropdownItemText}
        itemsContainerStyle={styles.dropdownItemsContainer}
        items={medicos.map(medico => ({
          id: medico.id,
          name: `${medico.nombre} ${medico.apPat} ${medico.apMat}`
        }))}
        defaultIndex={0}
        placeholder={selectedMedico || "Selecciona un médico"}
        resetValue={false}
        underlineColorAndroid="transparent"
      />
      <Button title="Crear Cita" onPress={handleCreateCita} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  input: {
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#ffffff',
  },
  selectedDate: {
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#ffffff',
    textAlignVertical: 'center',
    paddingTop: 10,
  },
  dropdownContainer: {
    marginBottom: 10,
  },
  dropdownInput: {
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
  },
  dropdownItem: {
    padding: 10,
    marginTop: 2,
    backgroundColor: '#ffffff',
    borderColor: '#bbb',
    borderWidth: 1,
    borderRadius: 5,
  },
  dropdownItemText: {
    color: '#333c87',
  },
  dropdownItemsContainer: {
    maxHeight: 140,
  },
});

export default CreateCita;
