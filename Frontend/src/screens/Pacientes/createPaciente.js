import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import { API_URL } from "../../config";
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const CreatePaciente = ({ route }) => {
  const { onRefresh } = route.params; // Obtener la función onRefresh de los parámetros de ruta
  const [nombre, setNombre] = useState('');
  const [apPat, setApPat] = useState('');
  const [apMat, setApMat] = useState('');
  const [fechaNacimiento, setFechaNacimiento] = useState('');
  const [direccion, setDireccion] = useState('');
  const [telefono, setTelefono] = useState('');
  const [correo, setCorreo] = useState('');
  const [clinicaId, setClinicaId] = useState(null); // Nuevo estado para almacenar el ID de la clínica
  const navigation = useNavigation();

  useEffect(() => {
    // Obtener el ID de la clínica del almacenamiento local
    const fetchClinicaId = async () => {
      try {
        const storedClinicaId = await AsyncStorage.getItem('clinica_id');
        if (storedClinicaId) {
          setClinicaId(storedClinicaId);
        }
      } catch (error) {
        console.error('Error al obtener el ID de la clínica:', error);
      }
    };

    fetchClinicaId(); // Obtener el ID de la clínica al cargar la pantalla
  }, []);

  const handleCreatePaciente = () => {
    const newPaciente = {
      nombre,
      apPat,
      apMat,
      fechaNacimiento,
      direccion,
      telefono,
      correo,
      clinica: clinicaId, // Añadir el ID de la clínica al nuevo paciente
    };

    axios.post(`http://${API_URL}:8000/api/core/pacientes/`, newPaciente)
      .then(response => {
        onRefresh(); // Actualizar la lista de pacientes en PacientesScreen
        navigation.goBack(); // Regresar a la pantalla anterior (PacientesScreen)
      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <View style={styles.container}>
      <TextInput style={styles.input} value={nombre} onChangeText={setNombre} placeholder="Nombre" />
      <TextInput style={styles.input} value={apPat} onChangeText={setApPat} placeholder="Apellido Paterno" />
      <TextInput style={styles.input} value={apMat} onChangeText={setApMat} placeholder="Apellido Materno" />
      <TextInput style={styles.input} value={fechaNacimiento} onChangeText={setFechaNacimiento} placeholder="Fecha de Nacimiento" />
      <TextInput style={styles.input} value={direccion} onChangeText={setDireccion} placeholder="Dirección" />
      <TextInput style={styles.input} value={telefono} onChangeText={setTelefono} placeholder="Teléfono" />
      <TextInput style={styles.input} value={correo} onChangeText={setCorreo} placeholder="Correo" />
      <TouchableOpacity style={styles.button} onPress={handleCreatePaciente}>
        <Text style={styles.buttonText}>Crear Paciente</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 50,
    backgroundColor: '#ffffff',
  },
  input: {
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#f0f0f0',
  },
  button: {
    backgroundColor: '#333c87',
    paddingVertical: 12,
    borderRadius: 8,
    alignItems: 'center',
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default CreatePaciente;
