import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TextInput, RefreshControl } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage'; // Importar AsyncStorage
import { API_URL } from "../../config";
import GreenButton from '../../components/greenButton';

const PacientesScreen = () => {
  const [pacientes, setPacientes] = useState([]);
  const [search, setSearch] = useState('');
  const [refreshing, setRefreshing] = useState(false);
  const [clinicaId, setClinicaId] = useState(null); // Nuevo estado para almacenar el ID de la clínica actual
  const navigation = useNavigation();

  const fetchPacientes = () => {
    if (clinicaId) {
      axios.get(`http://${API_URL}:8000/api/core/pacientes/`, {
        params: {
          clinica: clinicaId // Pasar el ID de la clínica como parámetro
        }
      })
        .then(response => {
          setPacientes(response.data);
          setRefreshing(false);
        })
        .catch(error => {
          console.error(error);
          setRefreshing(false);
        });
    }
  };

  useEffect(() => {
    const getClinicaIdFromStorage = async () => {
      try {
        const storedClinicaId = await AsyncStorage.getItem('clinica_id');
        if (storedClinicaId) {
          setClinicaId(storedClinicaId);
          fetchPacientes(); // Llamar a fetchPacientes después de establecer el ID de la clínica
        }
      } catch (error) {
        console.error('Error al obtener el ID de la clínica:', error);
      }
    };

    getClinicaIdFromStorage(); // Obtener el ID de la clínica al cargar la pantalla
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    fetchPacientes();
  };

  const filteredPacientes = pacientes.filter(paciente =>
    paciente.nombre.toLowerCase().includes(search.toLowerCase())
  );

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Text style={styles.title}>{item.nombre} {item.apPat} {item.apMat}</Text>
      <Text>{item.fechaNacimiento}</Text>
      <Text>{item.direccion}</Text>
      <Text>{item.telefono}</Text>
      <Text>{item.correo}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TextInput
          style={styles.searchBar}
          placeholder="Buscar paciente por nombre"
          value={search}
          onChangeText={setSearch}
        />
        <GreenButton
          title="Paciente Nuevo"
          onPress={() => navigation.navigate('CreatePaciente', { onRefresh: fetchPacientes })}
        />
      </View>
      <FlatList
        data={filteredPacientes}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  searchBar: {
    flex: 1,
    height: 40,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
    marginRight: 10,
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 2,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default PacientesScreen;
