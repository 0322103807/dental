import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Alert, StyleSheet, Image } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage'; // Importar AsyncStorage
import { API_URL } from "../config";

const ip = API_URL;

const storeClinicaId = async (clinica_id) => {
  try {
    await AsyncStorage.setItem('clinica_id', clinica_id.toString()); // Almacenar clinica_id como string
  } catch (e) {
    console.error(e);
  }
};

const storeMedicoId = async (medico_id) => {
  try {
    await AsyncStorage.setItem('medico_id', medico_id.toString()); // Almacenar medico_id como string
  } catch (e) {
    console.error(e);
  }
};



export default function LoginScreen() {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    axios.post(`http://${ip}:8000/api/core/login/`, { email, password })
      .then(response => {
        Alert.alert('Inicio de sesión exitoso', `Bienvenido ${response.data.email}`);
        const { clinica_id, is_medico, medico_id } = response.data;
        storeClinicaId(clinica_id); // Almacenar el clinica_id
        storeMedicoId(medico_id); // Almacenar el medico_id
        const userType = is_medico ? 'medico' : 'empleado';
        navigation.navigate('HomeTabs', { userType });
      })
      .catch(error => {
        Alert.alert('Error', 'Credenciales incorrectas');
      });
  };
  
  return (
    <View style={styles.container}>
      <Text style={styles.companyName}>Nombre de la Empresa</Text>
      <Image source={require('../assets/logo.png')} style={styles.logo} />
      <Text style={[styles.label, styles.blueText]}>Correo electrónico:</Text>
      <TextInput
        style={[styles.input, styles.blueBackground]}
        value={email}
        onChangeText={setEmail}
        keyboardType="email-address"
        placeholder="Ingrese su correo electrónico"
        placeholderTextColor="#ffffff"
      />
      <Text style={[styles.label, styles.blueText]}>Contraseña:</Text>
      <TextInput
        style={[styles.input, styles.blueBackground]}
        value={password}
        onChangeText={setPassword}
        secureTextEntry
        placeholder="Ingrese su contraseña"
        placeholderTextColor="#ffffff"
      />
      <TouchableOpacity onPress={handleLogin}>
        <Text style={styles.buttonText}>Iniciar sesión</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#afcdea',
  },
  companyName: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 60,
    color: '#333c87',
    lineHeight: 36,
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  blueText: {
    color: '#333c87',
  },
  input: {
    height: 40,
    width: '100%',
    backgroundColor: '#fff',
    borderColor: '#333c87',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
    color: '#ffffff',
    borderRadius: 8,
  },
  blueBackground: {
    backgroundColor: '#333c87',
    borderColor: '#333c87',
  },
  buttonText: {
    color: '#333c87',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 15,
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 30,
  },
});
