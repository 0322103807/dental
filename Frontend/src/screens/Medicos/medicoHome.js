import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function MedicoHome({ route }) {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <Text style={styles.welcomeText}>Bienvenido, Médico</Text>
            <TouchableOpacity 
                style={styles.button}
                onPress={() => navigation.navigate('SomeOtherScreen')}
            >
                <Text style={styles.buttonText}>Ir a otra pantalla</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#afcdea',
    },
    welcomeText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333c87',
        marginBottom: 10,
    },
    clinicaText: {
        fontSize: 18,
        color: '#333c87',
        marginBottom: 20,
    },
    button: {
        backgroundColor: '#333c87',
        padding: 10,
        borderRadius: 8,
    },
    buttonText: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: 'bold',
    },
});
