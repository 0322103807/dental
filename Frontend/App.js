import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LoginScreen from './src/screens/LoginScreen';
import EmpleadoHome from './src/screens/Empleados/empleadoHome';
import MedicoHome from './src/screens/Medicos/medicoHome';
import PacientesScreen from './src/screens/Pacientes/pacientesScreen';
import CreatePaciente from './src/screens/Pacientes/createPaciente';
import { StyleSheet} from 'react-native'; // Importamos StyleSheet, TextInput y TouchableOpacity desde react-native
import CitasScreen from './src/screens/Citas/citasScreen';
import CreateCita from './src/screens/Citas/createCita';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function HomeTabs({ route }) {
  const { userType } = route.params;

  return (
    <Tab.Navigator>
      {userType === 'medico' ? (
        <Tab.Screen 
          name="Home" 
          component={MedicoHome} 
          options={{ tabBarLabel: 'Home' }} 
        />
      ) : (
        <Tab.Screen 
          name="Home" 
          component={EmpleadoHome} 
          options={{ tabBarLabel: 'Home' }} 
        />
      )}
      <Tab.Screen 
        name="Pacientes" 
        component={PacientesScreen} 
        options={{ tabBarLabel: 'Pacientes' }} 
      />
      <Tab.Screen
        name="Citas"
        component={CitasScreen}
        options={{ tabBarLabel: 'Citas' }}
      />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen 
          name="Login" 
          component={LoginScreen} 
          options={{ headerShown: false }} 
        />
        <Stack.Screen 
          name="HomeTabs" 
          component={HomeTabs} 
          options={{ headerShown: false }} // Ocultar barra de navegación en HomeTabs
        />
        <Stack.Screen 
          name="CreatePaciente" 
          component={CreatePaciente} 
          options={{ headerBackVisible: false }} // Ocultar botón de retroceso en CreatePaciente
        />
        <Stack.Screen  
          name="Pacientes" 
          component={PacientesScreen} 
          options={{ headerBackVisible: false }} // Ocultar botón de retroceso en PacientesScreen
        />
        <Stack.Screen 
          name="MedicoHome" 
          component={MedicoHome} 
          options={{ headerBackVisible: false }} // Ejemplo para MedicoHome, ajustar según sea necesario
        />
        <Stack.Screen 
          name="EmpleadoHome" 
          component={EmpleadoHome} 
          options={{ headerBackVisible: false }} // Ejemplo para EmpleadoHome, ajustar según sea necesario
        />
        <Stack.Screen 
          name="CreateCita" 
          component={CreateCita} 
          options={{ headerBackVisible: false }} // Ocultar botón de retroceso en CreateCita
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    height: 40,
    width: 200,
    borderColor: '#333c87',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
  },
  newButton: {
    marginRight: 10,
    padding: 10,
  },
});

export default App;
